// tutorial instalasi mongodb 
// https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mywa";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  console.log("Database created!");
  var dbo = db.db("mywa"); 
  var myobj = { name: "Company Inc", address: "Highway 37" };
  dbo.collection("customers").insertOne(myobj, function(err, res) {
    if (err) throw err;
    console.log("1 document inserted");
    db.close();
  });
  db.close();
});