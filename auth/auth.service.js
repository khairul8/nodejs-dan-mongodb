const fetch = require('node-fetch');
const { apiUrl } = require('../config/keys')
//const { token } = require('../../config/keys')

function isAuth(req, res, next) {
    var token = req.get('X-AUTH-TOKEN');
    var projectId

    if (!token) return res.status(401).send({ status: false, error_code: 401, error_message: 'token empty'})

    fetch(`${apiUrl}device/info?token=${token}`, {
        method: 'GET'
    })
    .then(function(response) {
        return response.json();
    })
    .then(function(result) {
        if (result.status == true) {
            if (result.data.whatsapp.status == 'connected') {
                req.token = req.get('X-AUTH-TOKEN');
                next()
            } else {
                return res.status(403).send({ status: false, error_code: 403, error_message: 'token disconnect'})
            }
        } else {
            return res.status(401).send({ status: false, error_code: 401, error_message: 'token not found!'})
        }
    })
    .catch(function(err) {
        reject({ success : false, message: err})
    })
}

exports.isAuth = isAuth;