const express = require('express');
const router = express.Router();
const controller = require('./report.controller');
const auth = require('../../auth/auth.service');

router.get('/', auth.isAuth, controller.index);
router.get('/all', controller.all);

module.exports = router;
