const Messages = require('../messages/messages.model')
const Q = require('q')

exports.index = function (req, res) {

    if (!req.query.id) {

        Q.all(getById({token: req.get('X-AUTH-TOKEN')}).then(function(result) {
            res.status(200).send({status: true, message: 'success', timestamp: new Date(), result})
        }).catch(err => {
            res.status(500).send({status: false, message: 'failed', timestamp: new Date(), err})
        }))

    } else {

        Q.all(getById({messageId: req.query.id, token: req.get('X-AUTH-TOKEN')}).then(function(result) {
            res.status(200).send({status: true, message: 'success', timestamp: new Date(), result})
        }).catch(err => {
            res.status(500).send({status: false, message: 'failed', timestamp: new Date(), err})
        }))

    }

}

exports.all = function (req, res) {

    Q.all(getById().then(function(result) {
        res.status(200).send({status: true, message: 'success', timestamp: new Date(), result})
    }).catch(err => {
        res.status(500).send({status: false, message: 'failed', timestamp: new Date(), err})
    }))

}

function getById(data) {

    return new Promise((resolve, reject) => {

        Messages.find(data)
        .then(results => {
            resolve(results)
        }).catch(err => {
            reject(err);
        });

    })
}
