const express = require('express');
const router = express.Router();
const controller = require('./keyword.controller');
const auth = require('../../auth/auth.service');

router.get('/', auth.isAuth, controller.index);
router.post('/', auth.isAuth, controller.create);

module.exports = router;
