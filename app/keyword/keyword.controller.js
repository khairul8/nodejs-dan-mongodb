const Keyword = require('./keyword.model')
const moment = require('moment')
const Q = require('q')
const auth = require('../../auth/auth.service');

exports.index = function (req, res) {

    Messages.find()
    .then(results => {
        if (results.length == 0) res.status(403).send({status : true, message: 'message not found', timestamp: new Date()})
        res.status(200).send({status : true, message: 'message found', results})
    }).catch(err => {
        res.status(500).send({status : false, message: 'failed', timestamp: new Date(), err})
    });

}

exports.create = function (req, res) {

    
    
}

exports.detail = function (req, res) {

    Messages.find(req.body)
    .then(results => {
        if (results.length == 0) {
            return res.status(404).send({
                status : false,
                message : 'message not found'
            })
        }
        res.status(200).send({
            status : true,
            message : 'message found',
            timestamp : new Date(),
            results
        })
    }).catch(error => {
        return res.status(500).send({
            status : false,
            message : 'get message error',
            timestamp : new Date(),
            error
        })
    })
    
}
