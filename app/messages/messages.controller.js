const Messages = require('./messages.model')
const Users = require('../users/users.model')
const { apiUrl } = require('../../config/keys')
const moment = require('moment')
const Q = require('q')
const fetch = require('node-fetch')
const URLSearchParams = require('url-search-params')
const auth = require('../../auth/auth.service');

exports.index = function (req, res) {

    Messages.find()
    .then(results => {
        if (results.length == 0) res.status(403).send({status : true, message: 'message not found', timestamp: new Date()})
        res.status(200).send({status : true, message: 'message found', results})
    }).catch(err => {
        res.status(500).send({status : false, message: 'failed', timestamp: new Date(), err})
    });

}

exports.detail = function (req, res) {

    Messages.find(req.body)
    .then(results => {
        if (results.length == 0) {
            return res.status(404).send({
                status : false,
                message : 'message not found'
            })
        }
        res.status(200).send({
            status : true,
            message : 'message found',
            timestamp : new Date(),
            results
        })
    }).catch(error => {
        return res.status(500).send({
            status : false,
            message : 'get message error',
            timestamp : new Date(),
            error
        })
    })
    
}

exports.create = function (req, res) {

    if(!req.body.from || !req.body.to || !req.body.message || !req.body.type || !req.body.desc) {
        res.status(403).send({status : false, message: 'phone, message and type can not be empty', timestamp: new Date()});
    }
    
    var data = {
        phone : req.body.to,
        message : req.body.message,
        // schedule : req.body.schedule,
        token : req.get('X-AUTH-TOKEN')
    }
    Q.all(sendMessage(data).then(function(results) {
        //results.data.message.map(index, data)
        var msgId;
        results.data.message.forEach(function(data) {
            msgId = data.id;
        }) 

        const message = new Messages({
            messageId: msgId,  
            from: req.body.from,
            to: req.body.to,
            message: req.body.message,
            type: req.body.type,
            file: null,
            desc: req.body.desc,
            status : 'pending',
            log : null,
            schedule : null,
            token: req.get('X-AUTH-TOKEN'),
            createdAt: new Date()
        });
        message.save().then(result => {
            res.status(201)
            .send({
                status : true, 
                message : 'messages sent', 
                timestamp : new Date(), 
                results : {
                    messageId : msgId,
                    from : req.body.from,
                    to : req.body.to,
                    message : req.body.message,
                    type: req.body.type,
                    file : null,
                    desc : req.body.desc,
                    status : 'pending',
                    log : null,
                    schedule : null
                }
            })
        }).catch(err => {
            res.status(500).send({status : false, message : 'messages not sent', timestamp: new Date(), err})
        });
        
    }).catch(error => {
        res.status(500).send({status : false, message : 'messages not sent', timestamp: new Date(), error})
    }))
    
}

exports.report = function (req, res) {
    if (!req.body.from && !req.body.to) {
        var rpt = reportMessage(req.token)
    } else {
        var rpt = reportMessage(req.body.from, req.body.to)
    }
    Q.all(rpt).then(function(results) {
        res.status(200).send({status : true, mesage : 'report found', timestamp: new Date(), results})
    }, function(error) {
        res.status(500).send({status : false, message : 'failed get data', timestamp: new Date(), error})
    })
}

exports.status = function(req, res) {
    Q.all(getByMessageId(req.token, req.body.id).then(function(result) {
        var status = result.data.map(val => val.status)
        var log = result.data.map(val => val.log)
        var dataUpdate = {
            messageId : req.body.id,
            log : log.toString(),
            status : status.toString(),
        }
        Q.all(updateStatus(dataUpdate).then(function(resUpdate) {
            Q.all(getByMessageIdLokal(req.body.id).then(function(results) {
                res.status(200)
                .send({status : true, message : 'message found', timestamp: new Date(), results})
            }).catch(err => {
                res.status(500).send({status : false, message : 'failed get data', timestamp: new Date(), err})
            }))
        }).catch(error => {
            res.status(500).send({status : false, message : 'failed get data', timestamp: new Date(), error})
        }))
    }).catch(error => {
        res.status(500).send({status : false, message : 'failed get data', timestamp: new Date(), error})
    }))
}

exports.receive = function(req, res) {

    var token;
    Users.find({sender: req.body.receiver}).then(function(data) {
        token = data.token;
    })

    if (req.body.message == 'aseanindo') {
        var data = {
            phone : req.body.phone,
            message : 'Welcome to WhatsApp Business Aseanindo',
            token : token
        }
        Q.all(sendMessage(data).then(function(result) {
            console.log(result);
        }).catch(error => {
            console.log(error);
        }))
    } else if (req.body.message == 'kuy') {
        var data = {
            phone : req.body.phone,
            message : 'Kuylah sebats',
            token : token
        }
        Q.all(sendMessage(data).then(function(result) {
            console.log(result);
        }).catch(error => {
            console.log(error);
        }))
    } else {
        console.log(req.body)
    } 
}

exports.all = function(req, res) {

    var token;
    Users.find({sender: req.body.receiver}).then(function(data) {
        token = data.token;
    })

    if (req.body.message == 'aseanindo') {
        var data = {
            phone : req.body.phone,
            message : 'Welcome to WhatsApp Business Aseanindo',
            token : token
        }
        Q.all(sendMessage(data).then(function(result) {
            console.log(result);
        }).catch(error => {
            console.log(error);
        }))
    } else if (req.body.message == 'kuy') {
        var data = {
            phone : req.body.phone,
            message : 'Kuylah sebats',
            token : token
        }
        Q.all(sendMessage(data).then(function(result) {
            console.log(result);
        }).catch(error => {
            console.log(error);
        }))
    } else {
        console.log(req.body)
    } 
}

function sendMessage(data) {
    var params = new URLSearchParams();
    params.append('phone', data.phone);
    params.append('message', data.message);
    // var to;
    // if (data.schedule == 0) {
    //     to = 'send-message'
    // } else {
    //     var val = data.schedule.split(" ")
    //     params.append('date', val[0])
    //     params.append('time', val[1])
    //     to = 'schedule';
        
    // }
    return new Promise((resolve, reject) => {
        fetch(`${apiUrl}send-message`, {
            method: 'POST',
            body: params,
            headers: {
                'Authorization' : data.token,
                'Content-Type' : 'application/x-www-form-urlencoded;charset=UTF-8'
            }
        })
        .then(function(response) {
            return response.json();
        })
        .then(function(report) {
            resolve(report)
        })
        .catch(function(err) {
            reject({ success : false, message: err})
        })
    })
}

function updateMessage(data) {
    
    return new Promise((resolve, reject) => {
        Messages.findByIdAndUpdate(data.id, {
            messageId: data.messageId,
            status: data.status
        }, {useFindAndModify: false})
        .then(msgUpdate => {
            resolve(msgUpdate);
        }).catch(err => {
            if(err.kind === 'ObjectId') {  
                reject({ success : false, message: "Message not found with id "+ data.id})
            }
            reject({ success : false, message: "Error updating Message with id "+ data.id})
        })
    })
    
}

function updateStatus(data) {
    
    return new Promise((resolve, reject) => {
        Messages.updateOne(
            {messageId: data.messageId}, 
            {$set : {log: data.log, status: data.status}}, 
            {useFindAndModify: false})
        .then(msgUpdate => {
            resolve(msgUpdate);
        }).catch(err => {
            if(err.kind === 'ObjectId') {  
                reject({ success : false, message: "Message not found with id "+ data.id})
            }
            reject({ success : false, message: "Error updating Message with id "+ data.id})
        })
    })
    
}

function reportMessage(token, from, to) {
    if (!from && !to) {
        var api = `${apiUrl}report`
    } else {
        var api = `${apiUrl}report?from=${from}&to${to}`
    }
    return new Promise((resolve, reject) => {
        fetch(api, {
            method: 'GET',
            headers: {
                'Authorization' : token,    
            }
        })
        .then(function(response) {
            return response.json();
        })
        .then(function(report) {
            resolve(report)
        })
        .catch(function(err) {
            reject({ success : false, message: err})
        })
    })
}

function getByMessageId(token, id) {
    return new Promise((resolve, reject) => {
        fetch(`${apiUrl}report/${id}`, {
            method: 'GET',
            headers: {
                'Authorization' : token
            }
        })
        .then(function(response) {
            return response.json();
        })
        .then(data => {
            resolve(data);
            //console.log('data', data)
        })
        .catch(function(error) {
            //console.log('reject', error)
            reject({ success : false, message : error})
        })
    })
}

function getByMessageIdLokal(id) {

    return new Promise((resolve, reject) => {

        Messages.findOne({messageId: id})
        .then(results => {
            resolve(results)
        }).catch(err => {
            reject(err);
        });

    })
}
