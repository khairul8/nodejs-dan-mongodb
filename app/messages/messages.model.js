const mongoose = require('mongoose');
// const mongoosePaginate = require('mongoose-paginate');
// const Schema = mongoose.Schema;

const messagesSchema = mongoose.Schema({
    messageId : {
        type : String,
        required : true
    },
    from : {
        type : String,
        required : true
    },
    to : {
        type : String,
        required : true
    },
    message : {
        type : String,
        required : true
    },
    type : {
        type : String,
        required : true
    },
    file : {
        type : String,
        default : null
    },
    desc : {
        type : String,
        required : true
    },
    status : {
        type : String,
        required : true
    },
    log : {
        type : String,
        default : null
    },
    schedule : {
        type : String,
        default : null
    },
    token : {
        type : String,
        required : true
    },
    createdAt : {
        type : String,
        required : true
    }
})

// messagesSchema.plugin(mongoosePaginate)

// const Messages = mongoose.model("Messages", messagesSchema);

module.exports = mongoose.model('Messages', messagesSchema)