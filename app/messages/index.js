const express = require('express');
const router = express.Router();
const controller = require('./messages.controller');
const auth = require('../../auth/auth.service');

router.get('/', auth.isAuth, controller.index);
router.get('/all', auth.isAuth, controller.all);
router.post('/', auth.isAuth, controller.create);
router.post('/status', auth.isAuth, controller.status);
router.post('/detail', auth.isAuth, controller.detail);
router.post('/receive', controller.receive);

module.exports = router;
