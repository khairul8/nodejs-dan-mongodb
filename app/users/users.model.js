const mongoose = require('mongoose');

const usersSchema = mongoose.Schema({
    userId : {
        type : String,
        required : true
    },
    projectId : {
        type : String,
        required : true
    },
    productId : {
        type : String,
        required : true
    },
    sender : {
        type : String,
        required : true
    },
    pwd : {
        type : String,
        required : true
    },
    token : {
        type : String,
        required : true
    },
    expired : {
        type : String,
        required : true
    },
    createdAt : {
        type : Date,
        default : Date.now
    }
})

module.exports = mongoose.model('Users', usersSchema)