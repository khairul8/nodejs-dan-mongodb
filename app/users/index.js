const express = require('express');
const router = express.Router();
const controller = require('./users.controller');
const auth = require('../../auth/auth.service');

router.get('/', controller.index);
router.post('/', controller.create);

module.exports = router;
