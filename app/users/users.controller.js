const Users = require('./users.model')
const { apiUrl } = require('../../config/keys')
const moment = require('moment')
const Q = require('q')
const fetch = require('node-fetch')

exports.index = function (req, res) {

    if (!req.body.username || !req.body.password) return res.status(400).send({status : false, message: 'username or password be empty', timestamp: new Date()});

    Users.find({projectId: req.body.username, pwd: req.body.password}).then(data => {
        if (data.length == 0) return res.status(403).send({status : false, message: 'your username or password inccorect', timestamp: new Date()}); 
        res.status(200).send({ status: true, message: 'success', timestamp: new Date(), result: data})
    }).catch(err => {
        res.status(500).send({ status: false, message: 'token not found', timestamp: new Date(), err})
    })

}

exports.create = function (req, res) {

    Q.all(getInfo(req.get('X-AUTH-TOKEN'))).then(function(results) {
        if (results.status == false) return res.status(404).send({ status: false, message: 'token not found', timestamp: new Date(), error})
        Users.find({token: req.get('X-AUTH-TOKEN')}).then(data => {
            if (data.length == 0) {
                const users = new Users({
                    userId : results.data.user_id,
                    projectId : results.data.project_id,
                    productId : results.data.product_id,
                    sender : results.data.sender,
                    pwd : 'ctb01@kapita',
                    token : req.get('X-AUTH-TOKEN'),
                    expired : results.data.whatsapp.expired
                }).save().then(function(result) {
                    var data = [];
                    data.push(result);
                    res.status(200).send({ status: true, message:'token found', timestamp: new Date(), data})
                }).catch(err => {
                    res.status(500).send({ status: false, message: 'token not found', timestamp: new Date(), err})
                })
            } else {
                Users.updateOne(
                    {token: data.messageId}, 
                    {$set : {log: data.log, status: data.status}}, 
                    {useFindAndModify: false}
                ).then(msgUpdate => {
                    res.status(200).send({ status: true, message: 'token found', timestamp: new Date(), data})
                }).catch(err => {
                    if(err.kind === 'ObjectId') {  
                        res.status(500).send({ success : false, message: "Message not found with id "+ data.id})
                    }
                    res.status(500).send({ success : false, message: "Error updating Message with id "+ data.id})
                })
            }
        }).catch(err => {
            res.status(500).send({ status: false, message: 'token not found', timestamp: new Date(), err})
        })
    }).catch(function(error) {
        res.status(500).send({ status: false, message: 'token not found', timestamp: new Date()})
    })

}

function getInfo(token) {
    return new Promise((resolve, reject) => {
        fetch(`${apiUrl}device/info?token=${token}`, {
            method: 'GET'
        })
        .then(function(response) {
            return response.json();
        })
        .then(function(report) {
            resolve(report)
        })
        .catch(function(err) {
            console.log('err', err)
            reject({ success : false, message: err})
        })
    })
}
