module.exports = (app) => {
    app.use('/messages', require('./app/messages'));
    app.use('/users', require('./app/users'));
    app.use('/report', require('./app/report'));
    app.use('/keyword', require('./app/keyword'));
}
  