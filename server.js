const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const db = require('./config/keys');

const app = express();

mongoose.Promise = global.Promise;
mongoose.connect(db.url, { useNewUrlParser: true })
.then(() => {
    console.log("Success connect to database");    
}).catch(err => {
    console.log('Failed connect to database');
    process.exit();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

require('./routes')(app);

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`server running on port ${port}`));
